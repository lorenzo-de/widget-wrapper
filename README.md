# Widget wrapper

Widget wrapper built with React.

This doesn't do anything on its own but serves as a wrapper for React applications you want to expose as an embeddable widget.

Example:

Create your own widget and export both your widget and this wrapper:

```
import { widgetWrapper } from 'widget-wrapper'
import MyWidget from './MyWidget.jsx'

export const wrapper = widgetWrapper

export const myWidget = MyWidget
```

Then embed your wrapped widget like this:

```
<html>
  <head>
    <title>Widget Test Page</title>
  </head>
  <body id="body">
    <div id="widget"></div>
  </body>
  <script>
    tacticaltech.wrapper.new().render(myLib.myWidget())
  </script>
</html>
```

or using custom configuration options:

```tacticaltech.wrapper.new({locale: en_UK, selector: 'root'}).render(myLib.myWidget())```

Default configuration:

```
 defaultConfiguration = {
  selector: '#widget',
  locale: 'en_US'
}
```

## Run the application

Tested on Node 12.13.0

`npm install`

`npm run dev` runs a watching dev server with the app in development mode on port 8000. This builds a HTML stub in the output directory based on `src/index.html` and inserts the JS into it.

`npm run build` outputs the production built (JS code only) in the output directory.

`npm run test` to run unit tests.