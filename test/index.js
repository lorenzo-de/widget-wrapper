import assert from 'assert'
import _ from 'lodash'
import { defaultConfiguration } from '../index.js'

describe('default configuration', (done) => {
  it('should have a locale key which is a string', (done) => {
    assert.ok(_.isString(defaultConfiguration.locale))
    done()
  })
  it('should have a selector key which is a string', (done) => {
    assert.ok(_.isString(defaultConfiguration.selector))
    done()
  })
})
