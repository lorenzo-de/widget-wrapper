
const merge = require('webpack-merge')
const webpackConfig = require('./webpack.config')
const TerserPlugin = require('terser-webpack-plugin')

module.exports = merge(webpackConfig, {
  optimization: {
    minimizer: [
      new TerserPlugin()
    ]
  }
})
