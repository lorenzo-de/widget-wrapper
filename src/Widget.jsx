
import React from 'react'

export const Widget = ({children}) => {
  return (<div>{children}</div>)
}

export default Widget
