import '@babel/polyfill'
import 'raf/polyfill'

import React from 'react'
import ReactDOM from 'react-dom'

import Widget from './src/Widget.jsx'

export const defaultConfiguration = {
  selector: '#widget',
  locale: 'en_US'
}

export const widgetWrapper = {
  new: (config) => {
    let c
    if (config) c = config
    else c = defaultConfiguration

    if (c.selector) {
      return {
        render: (component) => {
          if (component) {
            ReactDOM.render(
              <Widget
                children={component}
                locale={c.locale || c.locale}
              />, document.querySelector(c.selector)
            )
          } else console.log('Error: you have to specify a component to render in the wrapper.')
        }
      }
    } else console.log('Error: you have to pass a selector for the widget in the configuration object.')
  }
}
