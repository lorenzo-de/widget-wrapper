const path = require('path')
const CopyPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const merge = require('webpack-merge')

const webpackConfig = require('./webpack.config')
const conf = require('./configuration')

module.exports = merge(webpackConfig, {
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(conf.appDir, 'index.html')
    }),
    new CopyPlugin([
      { from: path.join(__dirname, conf.appDir, 'index.html'), to: path.join(__dirname, '_site/index.html') }
    ])
  ],
  watch: true,
  devtool: 'eval-source-map',
  watchOptions: {
    aggregateTimeout: 600,
    ignored: /node_modules/
  },
  optimization: {
    minimize: false
  },
  devServer: {
    index: 'index.html',
    compress: false,
    port: conf.devPort
  }
})
