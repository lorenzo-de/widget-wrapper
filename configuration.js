module.exports = {
  // Application settings

  // The code root
  appDir: 'src',
  // The output directory
  outputDir: '_site',
  // The port when running in development mode
  devPort: 8000
}
